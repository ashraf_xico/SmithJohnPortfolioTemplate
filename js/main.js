
jQuery(document).ready(function(){
    'use strict';

// ################## info_counter js  start ###################### //

    (function ($) {
        $.fn.countTo = function (options) {
            options = options || {};

            return $(this).each(function () {
                // set options for current element
                var settings = $.extend({}, $.fn.countTo.defaults, {
                    from:            $(this).data('from'),
                    to:              $(this).data('to'),
                    speed:           $(this).data('speed'),
                    refreshInterval: $(this).data('refresh-interval'),
                    decimals:        $(this).data('decimals')
                }, options);

                // how many times to update the value, and how much to increment the value on each update
                var loops = Math.ceil(settings.speed / settings.refreshInterval),
                    increment = (settings.to - settings.from) / loops;

                // references & variables that will change with each update
                var self = this,
                    $self = $(this),
                    loopCount = 0,
                    value = settings.from,
                    data = $self.data('countTo') || {};

                $self.data('countTo', data);

                // if an existing interval can be found, clear it first
                if (data.interval) {
                    clearInterval(data.interval);
                }
                data.interval = setInterval(updateTimer, settings.refreshInterval);

                // initialize the element with the starting value
                render(value);

                function updateTimer() {
                    value += increment;
                    loopCount++;

                    render(value);

                    if (typeof(settings.onUpdate) == 'function') {
                        settings.onUpdate.call(self, value);
                    }

                    if (loopCount >= loops) {
                        // remove the interval
                        $self.removeData('countTo');
                        clearInterval(data.interval);
                        value = settings.to;

                        if (typeof(settings.onComplete) == 'function') {
                            settings.onComplete.call(self, value);
                        }
                    }
                }

                function render(value) {
                    var formattedValue = settings.formatter.call(self, value, settings);
                    $self.html(formattedValue);
                }
            });
        };

        $.fn.countTo.defaults = {
            from: 0,               // the number the element should start at
            to: 0,                 // the number the element should end at
            speed: 1000,           // how long it should take to count between the target numbers
            refreshInterval: 100,  // how often the element should be updated
            decimals: 0,           // the number of decimal places to show
            formatter: formatter,  // handler for formatting the value before rendering
            onUpdate: null,        // callback method for every time the element is updated
            onComplete: null       // callback method for when the element finishes updating
        };

        function formatter(value, settings) {
            return value.toFixed(settings.decimals);
        }
    }(jQuery));

    jQuery(function ($) {
        // custom formatting example
        $('.count-number').data('countToOptions', {
            formatter: function (value, options) {
                return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
            }
        });

        // start all the timers
        $('.timer').each(count);

        function count(options) {
            var $this = $(this);
            options = $.extend({}, options || {}, $this.data('countToOptions') || {});
            $this.countTo(options);
        }
    });

// ################## info_counter js  end ###################### //

    
// ################## mixitUp js  start ###################### //    

        $(function(){
        // Instantiate MixItUp:
        $('.sort').mixItUp();
    });

// ################## mixitUp js  end ###################### //  


// ################## owl carousel js  start ###################### //

    $(document).ready(function(){
    $('.owl-carousel').owlCarousel({   
    loop:true,
    margin:10,
    nav:true,
    navText: ["<div class='limit_nav_left'><i class='fa fa-arrow-left fa-2x'></i></div>",
    "<div class='limit_nav_right'><i class='fa fa-arrow-right fa-2x'></i></div>"],
    autoplay:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
        }
    })
    });  

// ################## owl carousel js end ###################### //



// ################## smooth scroll js start ###################### //

    $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });

// ################## smooth scroll js end ###################### //


// ################## typed js start ###################### //

    $(".typed").typed({
        strings: ["Beautifull Map.", "WebGigs.", "Geodatabse.", "Spatial Analyst" ],
        typeSpeed: 1,
        loop: true,
        backDelay: 1000
      });

// ################## type js end ###################### //



// ################## sticky menu js start ###################### //


        function stickyMenu(){
        $(window).on('scroll', function(){
            var x = $(this).scrollTop();

            if(x > 100){
                $('.navbar-default').addClass('isActive');
            }else{
                $('.navbar-default').removeClass('isActive');
            }
        });
    }
    stickyMenu();

// ################## sticky menu js end ###################### //



// ################## scrollspy js start ###################### //


    $('body').scrollspy({ target: '.navbar-ex1-collapse' });
    $('body').scrollspy({ target: '.navbar-ex2-collapse' });


// ################## scrollspy js end ###################### //


});






