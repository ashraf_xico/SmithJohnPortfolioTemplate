// ################## skill progress bar js  start ###################### //

    document.querySelector('#p3').addEventListener('mdl-componentupgraded', function() {
        this.MaterialProgress.setProgress(53);
        this.MaterialProgress.setBuffer(87);
    });


    document.querySelector('#p4').addEventListener('mdl-componentupgraded', function() {
        this.MaterialProgress.setProgress(73);
        this.MaterialProgress.setBuffer(87);
    });


    document.querySelector('#p5').addEventListener('mdl-componentupgraded', function() {
        this.MaterialProgress.setProgress(63);
        this.MaterialProgress.setBuffer(87);
    });

    document.querySelector('#p6').addEventListener('mdl-componentupgraded', function() {
        this.MaterialProgress.setProgress(83);
        this.MaterialProgress.setBuffer(87);
    });


    document.querySelector('#p7').addEventListener('mdl-componentupgraded', function() {
        this.MaterialProgress.setProgress(73);
        this.MaterialProgress.setBuffer(87);
    });


    document.querySelector('#p8').addEventListener('mdl-componentupgraded', function() {
        this.MaterialProgress.setProgress(53);
        this.MaterialProgress.setBuffer(87);
    });

// ################## skill progress bar js end ###################### //
